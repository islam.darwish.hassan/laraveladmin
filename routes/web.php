<?php

use App\Http\Controllers\Admin\BusinessController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\HomeController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Auth\LoginController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('home');

//auth routes
Route::group([],function(){
Route::get('/login',[LoginController::class,'login_form'])->name('login');
Route::post('/login',[LoginController::class,'login'])->name('login.store');
Route::post('/logout',[LoginController::class,'logout'])->name('logout');


});

// auth required routes
Route::group(['middleware'=>'auth'],function(){
    //admin routes
    Route::group(['middleware'=>'is_Admin','prefix'=>'admin'],function(){
        Route::get('/',[HomeController::class,'index'])->name('admin.home');

        Route::resource('/users',UserController::class);
        Route::resource('/categories',CategoryController::class);
        Route::resource('/businesses',BusinessController::class);
    });

});
