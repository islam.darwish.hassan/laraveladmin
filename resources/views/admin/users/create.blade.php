@extends('layouts.resources.create')

@section('form')
@include('_includes.message')

<form class="d-flex flex-column " method="POST" action="{{route('users.store')}}">
    @csrf
    <div class="form-group">
        <label for="exampleFormControlInput1">User name</label>
        <input type="name" name="name"  class="form-control" id="exampleFormControlInput1" placeholder="example">
      </div>

    <div class="form-group">
      <label for="exampleFormControlInput1">Email address</label>
      <input type="email" name="email" class="form-control" id="exampleFormControlInput1" placeholder="name@example.com">
    </div>

    <div class="form-group">
        <label for="exampleInputPassword1">Password</label>
        <input type="password" name="password" class="form-control" id="exampleInputPassword1">
      </div>

    <div class="form-group">
      <label for="exampleFormControlSelect1">Select Role</label>
      <select class="form-control" name="role" id="role">
        <option selected >-- Choose --</option>
        <option value="1" >User</option>
        <option value="2">Admin</option>
      </select>
    </div>

    <button type="submit" class="btn btn-primary align-self-end">Submit</button>

</form>
@endsection
