@extends('layouts.resources.index')


@section('header')
<nav class="navbar d-flex justify-content-between navbar-expand-lg navbar-light bg-light  align-content-justify-end">
    <a class="navbar-brand " href="#">User Management</a>

        <a type="submit" class="btn btn-primary   align-self-end"  href="{{route('users.create')}}">Add User</a>

</nav>
@endsection
@section('table-header')
        <tr>
        <th scope="col">#</th>
        <th scope="col">Name</th>
        <th scope="col">Email</th>
        <th scope="col">User Role</th>
        <th scope="col">Joined</th>
        <th scope="col">Options</th>
        </tr>
@endsection



@section('table-body')
            @foreach ($data as $item)
           <tr>
                <td>{{$item->id}}</td>
             <td>{{$item->name}}</td>
             <td>{{$item->email}}</td>
            <td>{{$item->role}}</td>
           <td>{{$item->created_at}}</td>
           <td>
            <form method="POST"
            action="{{route('users.edit',$item)}}">
             @csrf
             @method('GET')
             <button type="submit" class="btn btn-success   align-self-end">Edit</button>
            </form>

                <form method="POST"
                action="{{route('users.destroy',$item->id)}}">
                 @csrf
                 @method('DELETE')
                <button type="submit" class="btn btn-danger   align-self-end">Delete</button>
            </form>


           </td>

            </tr>


            @endforeach

@endsection
