@extends('layouts.resources.update')

@section('form')
@include('_includes.message')
@php

@endphp
 <form class="d-flex flex-column "  method="POST" action="{{route('users.update', $user->id)}}">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label for="exampleFormControlInput1">User name</label>
        <input type="name" name="name"  class="form-control" id="exampleFormControlInput1" placeholder="{{$user->name}}">

      </div>

    <div class="form-group">
      <label for="exampleFormControlInput1">Email address</label>
      <input type="email" name="email" class="form-control" id="disabledTextInput"  disabled placeholder="{{$user->email}}"
        value="{{$user->email}}">
     </div>



    <div class="form-group">
      <label for="exampleFormControlSelect1">Select Role</label>
      <select class="form-control" name="role" id="role">
        <option selected >-- Choose --</option>
        <option value="1" >User</option>
        <option value="2">Admin</option>
      </select>
    </div>

    <button type="submit" class="btn btn-primary align-self-end">Update</button>

</form>
 @endsection
