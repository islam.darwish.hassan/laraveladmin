@extends('layouts.resources.index')


@section('header')
<nav class="navbar d-flex justify-content-between navbar-expand-lg navbar-light bg-light  align-content-justify-end">
    <a class="navbar-brand " href="#">Categories Management</a>

        <a type="submit" class="btn btn-primary   align-self-end"  href="{{route('categories.create')}}">Add Category</a>

</nav>
@endsection
@section('table-header')
        <tr>
        <th scope="col">#</th>
        <th scope="col">Name</th>
        <th scope="col">ar_name</th>
        <th scope="col">Joined</th>
        <th scope="col">Options</th>
        </tr>
@endsection



@section('table-body')
            @foreach ($data as $item)
           <tr>
                <td>{{$item->id}}</td>
             <td>{{$item->name}}</td>
             <td>{{$item->ar_name}}</td>
            <td>{{$item->created_at}}</td>
           <td>
            <form method="POST"
            action="{{route('categories.edit',$item)}}">
             @csrf
             @method('GET')
             <button type="submit" class="btn btn-success   align-self-end">Edit</button>
            </form>

                <form method="POST"
                action="{{route('categories.destroy',$item->id)}}">
                 @csrf
                 @method('DELETE')
                <button type="submit" class="btn btn-danger   align-self-end">Delete</button>
            </form>


           </td>

            </tr>


            @endforeach

@endsection
