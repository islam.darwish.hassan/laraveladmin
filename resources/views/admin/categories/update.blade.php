@extends('layouts.resources.update')

@section('form')
@include('_includes.message')
@php

@endphp
 <form class="d-flex flex-column "  method="POST" action="{{route('categories.update', $category->id)}}">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label for="exampleFormControlInput1">Category name</label>
        <input type="name" name="name"  class="form-control" id="exampleFormControlInput1" placeholder="{{$category->name}}">

      </div>

      <div class="form-group">
        <label for="exampleFormControlInput1">Category arabic name</label>
        <input type="name" name="ar_name"  class="form-control" id="exampleFormControlInput1" placeholder="{{$category->ar_name}}">

      </div>



    <button type="submit" class="btn btn-primary align-self-end">Update</button>

</form>
 @endsection
