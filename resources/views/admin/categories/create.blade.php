@extends('layouts.resources.create')

@section('form')
@include('_includes.message')

<form class="d-flex flex-column " method="POST" action="{{route('categories.store')}}">
    @csrf
    <div class="form-group">
        <label for="exampleFormControlInput1">Category name</label>
        <input type="name" name="name"  class="form-control" id="exampleFormControlInput1" placeholder="example">
      </div>

    <div class="form-group">
        <label for="exampleFormControlInput1">Category arabic name</label>
        <input type="name" name="ar_name"  class="form-control" id="exampleFormControlInput1" placeholder="example">
      </div>






    <button type="submit" class="btn btn-primary align-self-end">Submit</button>

</form>
@endsection
