@extends('layouts.resources.update')

@section('form')
@include('_includes.message')
@php

@endphp
 <form class="d-flex flex-column "  method="POST" action="{{route('businesses.update', $business->id)}}">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label for="exampleFormControlInput1">Business name</label>
        <input type="name" name="name"  class="form-control" id="exampleFormControlInput1" placeholder="{{$business->name}}">

      </div>

      <div class="form-group">
        <label for="exampleFormControlInput1">Business arabic name</label>
        <input type="name" name="ar_name"  class="form-control" id="exampleFormControlInput1" placeholder="{{$business->ar_name}}">


        <div class="form-group">
            <label for="exampleFormControlSelect1">Select business type </label>
            <select class="form-control" name="type" id="role">
              <option selected >-- Choose --</option>
              <option value="1" >General </option>
              <option value="2">Resturant</option>
            </select>
          </div>


          <div class="form-group">
            <label for="exampleFormControlSelect1">Select business status  </label>
            <select class="form-control" name="status" id="role">
              <option selected >-- Choose --</option>
              <option value="1" >Accepted </option>
              <option value="2">Verified</option>
              <option value="3">Suspended</option>

            </select>
          </div>




      </div>






    <button type="submit" class="btn btn-primary align-self-end">Update</button>

</form>
 @endsection
