@extends('layouts.resources.create')

@section('form')
@include('_includes.message')

<form class="d-flex flex-column " method="POST" action="{{route('businesses.store')}}">
    @csrf
    <div class="form-group">
        <label for="exampleFormControlInput1">Bussines name</label>
        <input type="name" name="name"  class="form-control" id="exampleFormControlInput1" placeholder="example">
      </div>

    <div class="form-group">
        <label for="exampleFormControlInput1">Bussines arabic name</label>
        <input type="name" name="ar_name"  class="form-control" id="exampleFormControlInput1" placeholder="example">
      </div>


      <div class="mb-3">
        <label for="exampleFormControlTextarea1" class="form-label">Description </label>
        <textarea class="form-control" name='description' id="exampleFormControlTextarea1" rows="3"></textarea>
      </div>

      <div class="mb-3">
        <label for="exampleFormControlTextarea1" class="form-label">Arabic description </label>
        <textarea class="form-control" name='ar_description' id="exampleFormControlTextarea1" rows="3"></textarea>
      </div>


      <div class="mb-3">
        <label for="formFileMultiple" class="form-label">Business avatar</label>
        <input class="form-control" name="avatar" type="file" id="formFileMultiple" multiple>
      </div>


      <div class="form-group">
        <label for="exampleFormControlSelect1">Avatar type </label>
        <select class="form-control" name="avatar_type" id="role">
          <option selected >-- Choose --</option>
          <option value="1" >URL </option>
          <option value="2">Storage</option>
        </select>
      </div>



      <div class="form-group">
        <label for="exampleFormControlSelect1">Select business type </label>
        <select class="form-control" name="type" id="role">
          <option selected >-- Choose --</option>
          <option value="1" >General </option>
          <option value="2">Resturant</option>
        </select>
      </div>


      <div class="form-group">
        <label for="exampleFormControlSelect1">Select business status  </label>
        <select class="form-control" name="status" id="role">
          <option selected >-- Choose --</option>
          <option value="1" >Accepted </option>
          <option value="2">Verified</option>
          <option value="3">Suspended</option>

        </select>
      </div>





    <button type="submit" class="btn btn-primary align-self-end">Submit</button>

</form>
@endsection
