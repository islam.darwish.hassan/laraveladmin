<div class="conatiner">

    <div class="mt-2">
        @if(session('message'))
        <div class="alert alert-success" role="alert">
            <b>{{session('message')}}</b>
        </div>
        @endif
        @if(session('erorr'))
        <div class="alert alert-danger" role="alert">
            <b>{{session('error')}}</b>
        </div>
        @endif

        @if($errors->any())
        <div class="alert alert-danger" role="alert">
            <p> <b>Please fix these errors </b></p>
           <ul>
               @foreach ($errors->all() as $error)
                   <li>{{$error}}</li>
               @endforeach
           </ul>
        </div>
        @endif
     </div>


</div>
