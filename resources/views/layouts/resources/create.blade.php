@extends('layouts.admin')

@section('content')
    <div class="conatiner-fluid py-5 px-5">
        <div class="card  mx-5 my-3">
            <div class="card-header  ">
                Add New
            </div>
            <div class="card-body  ">
                @yield('form')

            </div>
        </div>
    </div>

@endsection

