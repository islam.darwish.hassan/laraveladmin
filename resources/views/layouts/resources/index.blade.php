@extends('layouts.admin')
@section('content')
@yield('header')
@include('_includes.message')

<div class="container-fluid my-2">
    <table class="table  table-striped table-dark">
        <thead>
          <tr>
                @yield('table-header')
           </tr>
        </thead>
        <tbody>
            @yield('table-body')
        </tbody>
      </table>



</div>
<div class="container-fluid d-flex justify-content-center">
    {{$data->links('vendor\pagination\simple-bootstrap-4')}}
</div>
@endsection
