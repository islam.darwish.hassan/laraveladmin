<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin Panel</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="#">AdminPanel</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
          <div class="navbar-nav">
            <a class="nav-link  @if  (request()->routeIs('admin.home')) active @endif " href="{{route('admin.home')}}">Home</span></a>
            <a class="nav-link @if  (request()->routeIs('users.index')) active @endif "   href="{{route('users.index')}}">Users Managment</a>
            <a class="nav-link @if  (request()->routeIs('categories.index')) active @endif "   href="{{route('categories.index')}}">Categories Managment</a>
            <a class="nav-link"@if  (request()->routeIs('businesses.index')) active @endif "  href="{{route('businesses.index')}}">Businesses Managment</a>
          </div>
        </div>
        <form action="{{route('logout')}}" method="POST">
            @csrf
            <button type="submit" class="btn btn-danger">Logout</button>
        </form>

      </nav>
      <div class="container-fluid">
        @yield('content')
      </div>
      </body>
<script src="{{asset('js/app.js')}}"></script>
</html>
