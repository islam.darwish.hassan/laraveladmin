<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login To Admin</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
</head>
<body>
    <div class="container p-5">
        <div class="card">
            <div class="card-header">
                Login To Admin
            </div>
            <div class="card-body">
                @if(session('message'))
                <div class="alert alert-dark mt-2">{{ session('message') }}</div>
                
                @endif
                <form action="{{route('login.store')}}" method="POST">
                @csrf
                <div class="form-group">
                    <label for="email">Email</label>
                    <input id="email" class="form-control" type="email" name="email" required>
                    @error('email')
                     <div class="alert alert-danger mt-2">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input id="password" class="form-control" type="password" name="password" required>
                    @error('password')
                    <div class="alert alert-danger mt-2">{{ $message }}</div>
                   @enderror
                </div>
                <button type="submit" class="btn btn-success btn-block mt-5">Login</button>
                </form>
    
            </div>
        </div>
    
    </div>
</body>
<script src="{{asset('js/app.js')}}"></script>
</html>