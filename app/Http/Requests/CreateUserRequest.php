<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'name'          => 'required|min:2|max:20',
            'email'         => 'required|unique:users|max:190|email',
            'password'      => 'required|max:190|min:6|alpha_num',
            'role'          => 'required|integer|between:1,4',
        ];
    }
}
