<?php

namespace App\Http\Requests;

use App\Models\SubCategory;
use Illuminate\Foundation\Http\FormRequest;

class UpdateBusinessRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $sub_categories = SubCategory::all()->implode('id', ',');

        return [
            //
            'sub_category'               => 'in:' . $sub_categories,
            'name'                        => 'required|min:3|max:500',
            'ar_name'                      => 'required|min:3|max:500',
            'type'                        => 'required|integer|between:1,2',
            'status'                       => 'required|integer|between:1,3',

            'description'                => 'nullable|min:30|max:500',
            'ar_description'             => 'nullable|min:30|max:500',

            'address'                     => 'nullable|min:30|max:500',

            'phone'                      => 'nullable|max:12|min:7',

            'avatar'                   => 'nullable|image|dimensions:ratio=1/1|dimensions:min_width=511,min_height=511|dimensions:max_width=2049,max_height=2049',

            'avatar_type'             => 'nullable|integer|between:1,2',
             'website '               =>'nullable|min:5|max:250',



        ];
    }

}
