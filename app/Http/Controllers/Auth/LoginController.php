<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    //
    public function login_form()
    {
        # code...
        return view('auth.login');
    }

    public function login(LoginRequest $request)
    {
        # code....
        $user= User::where('email',strtolower($request->email))->first();
    
        if(!$user) return back()->with(['message'=>'Wrong Cred!']);
        else{
            if(!Hash::check($request->password, $user->password))
            return back()->with(['message'=>'Wrong Cred!']);
        }

        Auth::login($user);
        
        if($user->role ==User::ADMIN)
            return redirect()->route('admin.home');


        
        return back();
    }
    public function logout()
    {
        Auth::logout();
        return redirect()->route('home');

    }

}
