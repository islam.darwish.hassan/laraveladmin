<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;

use App\Models\User as ModelsUser;
use DeepCopy\Filter\Filter;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    //

  /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ModelsUser $user, Request $request)
    {

        // $users = $user;
        //  if ($request->query('role') && $request->query('role') != 'none') {
        //     $users = $users->where('role', $request->query('role'));
        // }
        $users = User::orderBy('created_at', 'desc')->paginate(8);
        $name = 'users';
        $data = $users;

        return view('admin.users.index', compact('users', 'name', 'data'));
    }

      /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function create(Request $request)
    {

        return view('admin.users.create');
    }


      /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    public function store(CreateUserRequest $request)
    {

       // return $request;


        $user = new User();
        $user->name    = $request->input('name');
        $user->email    = $request->input('email');
        $user->password = bcrypt($request->password);
        $user->role     = $request->input('role');
        $user->save();

        return redirect()->route('users.index')->with('message', 'User created successfully');

    }



      /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(ModelsUser $user)
    {

        $name = 'users';

        return view('admin.users.update', compact('user', 'name'));
    }





       /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, User $user)
    {

       // return($request);
        $user->save();
        return redirect()->route('users.index')->with('message', 'User info updated');
    }




     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(ModelsUser $user)
    {



        if ($user->role == 2) {
            $user->delete();
            return redirect()->back()->with('message', 'Admin deleted successfully');
        } elseif ($user->role == 1) {
            $user->delete();
            return redirect()->back()->with('message', 'User deleted successfully');
        }else{
            $user->delete();
            return redirect()->back()->with('message', 'User deleted successfully');

        }
    }



}
