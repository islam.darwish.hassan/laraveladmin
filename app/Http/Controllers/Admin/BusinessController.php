<?php


namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateBusinessRequest;
use App\Http\Requests\CreateCategoryRequest;
use App\Http\Requests\UpdateBusinessRequest;
use App\Http\Requests\UpdateCategoryRequest;
use Illuminate\Http\Request;
use App\Models\Business as ModelBusiness;

class BusinessController extends Controller
{
    //

/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ModelBusiness $user, Request $request)
    {


        $Businesses = ModelBusiness::orderBy('created_at', 'desc')->paginate(8);
        $name = 'Businesses';
        $data = $Businesses;

        return view('admin.Businesses.index', compact('Businesses', 'name', 'data'));
    }


  /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        return view('admin.Businesses.create');
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateBusinessRequest $request)
    {

       // return $request;


        $business = new ModelBusiness();
        $business->name    = $request->input('name');
        $business->ar_name    = $request->input('ar_name');
        $business->type   = $request->input('type');
        $business->status   = $request->input('status');

        $business->description   = $request->input('description');
        $business->ar_description   = $request->input('ar_description');
        $business->address   = $request->input('address');
        $business->phone   = $request->input('phone');
        $business->avatar   = $request->input('avatar');

        $business->avatar_type   = $request->input('avatar_type');
        $business->website   = $request->input('website');

        $business->sub_category_id   = $request->input('sub_category_id');

        $business->save();

        return redirect()->route('businesses.index')->with('message', 'Business created successfully');

    }


     /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, ModelBusiness $business)
    {
        $this->authorize('view', $business);
        $products=ModelBusiness::where('id', $business->id)->orderBy('created_at','desc')->limit(6)->get();
        $name     = 'business';

        return view('admin.businesses.show', compact('business','name'));
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(ModelBusiness $business)
    {

        $name = 'businesses';

        return view('admin.businesses.update', compact('business', 'name'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBusinessRequest $request,ModelBusiness $business)
    {
        //
        $business->save();
        return redirect()->route('businesses.index')->with('message', 'Business info updated');
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(ModelBusiness $business)
    {

            $business->delete();
            return redirect()->back()->with('message', 'Business deleted successfully');


    }







}
