<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateCategoryRequest;
use App\Http\Requests\UpdateCategoryRequest;
use Illuminate\Http\Request;
use App\Models\Category as ModelCategory;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ModelCategory $user, Request $request)
    {


        $categories = ModelCategory::orderBy('created_at', 'desc')->paginate(8);
        $name = 'categories';
        $data = $categories;

        return view('admin.categories.index', compact('categories', 'name', 'data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        return view('admin.categories.create');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCategoryRequest $request)
    {

       // return $request;


        $category = new ModelCategory();
        $category->name    = $request->input('name');
        $category->ar_name    = $request->input('ar_name');

        $category->save();

        return redirect()->route('categories.index')->with('message', 'Category created successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(ModelCategory $category)
    {

        $name = 'categories';

        return view('admin.categories.update', compact('category', 'name'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCategoryRequest $request,ModelCategory $category)
    {
        //
        $category->save();
        return redirect()->route('categories.index')->with('message', 'Category info updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(ModelCategory $category)
    {

            $category->delete();
            return redirect()->back()->with('message', 'Category deleted successfully');


    }

}
