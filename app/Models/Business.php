<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Business extends Model
{
    use HasFactory;
    //business types
    const TYPE_GENERAL =1; 
    const TYPE_RESTAURANT=2;
    //business statuses
    const STATUS_ACCEPTED=1;
    const STATUS_VERIFIED=2;
    const STATUS_SUSPENDED=3;
    //avatar types
    const AVATAR_URL=1;
    const AVATAR_STORAGE=2;
    public function sub_category()
    {
        return $this->belongsTo('App\Models\SubCategory','sub_category_id');
    }

    public function available()
    {
        return $this->where('status','!=',BUSINESS::STATUS_SUSPENDED);
    }

}
