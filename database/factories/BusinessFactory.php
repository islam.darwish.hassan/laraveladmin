<?php

namespace Database\Factories;

use App\Models\Business;
use App\Models\SubCategory;
use Illuminate\Database\Eloquent\Factories\Factory;

class BusinessFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Business::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $sub_category=SubCategory::inRandomOrder()->first();
        return [
            //

            'name'=>$this->faker->company(),
            'ar_name'=>'بالعربى'. rand(1,10000),
            'type'=>rand(1,2),
            'status'=>rand(1,3),
            'description'=>$this->faker->sentence(),
            'ar_description'=>'الجملة بالعرب هنا ',
            'address'=>$this->faker->address(),
            'phone'=>$this->faker->phoneNumber(),
            'website'=>$this->faker->url(),
            'avatar'=>$this->faker->imageUrl(500,500),
            'sub_category_id'=>$sub_category->id
        ];
    }
}
